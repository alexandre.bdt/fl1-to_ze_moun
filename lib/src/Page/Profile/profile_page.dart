import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:to_ze_moun/src/Services/auth.dart';
import 'package:to_ze_moun/src/Services/user.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final AuthService _auth = AuthService();
  final ImagePicker _picker = ImagePicker();
  final UserService _user = UserService();
  late String _profilePic = "";

  @override
  void initState() {
    super.initState();
    getProfilePicture();
  }

  Future uploadFile() async {
    final image = await _picker.pickImage(source: ImageSource.camera);
    if (image == null) return;
    File file = File(image.path);
    await _user.uploadUserProfilePicture(file);
    getProfilePicture();
  }

  Future getProfilePicture() async {
    String? img = await _user.getUserProfilePicture();
    if (img == null) return;
    setState(() {
      _profilePic = img;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("To ze moun"),
        actions: [
          ElevatedButton(
            onPressed: () async => {await _auth.signout()},
            child: const Text("Signout"),
            style: ElevatedButton.styleFrom(
              shadowColor: Colors.black.withOpacity(0.0),
              elevation: 0.0,
            ),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          RawMaterialButton(
            onPressed: uploadFile,
            child: const Icon(
              Icons.upload_file_rounded,
              size: 40,
            ),
          ),
          CircleAvatar(
            backgroundColor: Colors.blue,
            radius: 100,
            backgroundImage:
                (_profilePic == "") ? null : NetworkImage(_profilePic),
          )
        ],
      ),
    );
  }
}
