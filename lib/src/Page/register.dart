import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:to_ze_moun/src/Services/auth.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final AuthService _auth = AuthService();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmController =
      TextEditingController();
  String error = '';

  void goBack() {
    Navigator.pop(context);
  }

  Widget _usernameTextarea() {
    return TextField(
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        hintText: 'Email',
        fillColor: Colors.green,
      ),
      style: const TextStyle(
        fontSize: 20,
        color: Colors.black,
      ),
      controller: _emailController,
    );
  }

  Widget _passwordTextarea() {
    return TextField(
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        hintText: 'password',
        fillColor: Colors.green,
      ),
      style: const TextStyle(
        fontSize: 20,
        color: Colors.black,
      ),
      controller: _passwordController,
      obscureText: true,
      enableSuggestions: false,
      autocorrect: false,
    );
  }

  Widget _confirmPasswordTextarea() {
    return TextField(
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        hintText: 'password',
        fillColor: Colors.green,
      ),
      style: const TextStyle(
        fontSize: 20,
        color: Colors.black,
      ),
      controller: _passwordConfirmController,
      obscureText: true,
      enableSuggestions: false,
      autocorrect: false,
    );
  }

  Widget _rowButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        ElevatedButton(
          onPressed: () async {
            if (_passwordConfirmController.text != _passwordController.text) {
              setState(() => error = "Password does not match");
              return;
            }
            dynamic res = await _auth.registerWithEmail(
                _emailController.text, _passwordController.text);
            if (res == null) {
              setState(() => {error = "An error occured"});
            } else if (res != null) {
              if (res is UserCredential) {
                await _auth.createUserInDb(_emailController.text, res);
              }
            }
          },
          child: const Text("Register",
              style: TextStyle(
                color: Colors.black,
              )),
          style: ElevatedButton.styleFrom(
            primary: Colors.pink[200],
          ),
        ),
        ElevatedButton(
          onPressed: goBack,
          child: const Text("Back",
              style: TextStyle(
                color: Colors.black,
              )),
          style: ElevatedButton.styleFrom(
            primary: Colors.red[400],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        const Text(
          "Register",
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.bold,
            decoration: TextDecoration.underline,
            color: Colors.lightGreenAccent,
          ),
        ),
        SizedBox(
          width: 300,
          child: _usernameTextarea(),
        ),
        const SizedBox(height: 10),
        SizedBox(
          width: 300,
          child: _passwordTextarea(),
        ),
        const SizedBox(height: 10),
        SizedBox(
          width: 300,
          child: _confirmPasswordTextarea(),
        ),
        _rowButton(),
        Text(error, style: const TextStyle(color: Colors.red, fontSize: 16))
      ],
    ));
  }
}
