import 'package:flutter/material.dart';
import 'package:to_ze_moun/src/Services/auth.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final AuthService _auth = AuthService();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  String error = '';

  void goBack() {
    Navigator.pop(context);
  }

  Widget _usernameTextarea() {
    return TextField(
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        hintText: 'Email',
        fillColor: Colors.green,
      ),
      style: const TextStyle(
        fontSize: 20,
        color: Colors.black,
      ),
      controller: _emailController,
    );
  }

  Widget _passwordTextarea() {
    return TextField(
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        hintText: 'password',
        fillColor: Colors.green,
      ),
      style: const TextStyle(
        fontSize: 20,
        color: Colors.black,
      ),
      controller: _passwordController,
      obscureText: true,
      enableSuggestions: false,
      autocorrect: false,
    );
  }

  Widget _rowButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        ElevatedButton(
          onPressed: () async {
            if (_emailController.text.isEmpty ||
                _passwordController.text.isEmpty) return;
            await _auth.signInWithEmail(
                _emailController.text, _passwordController.text);
            _auth.setUserName(_emailController.text);
          },
          child: const Text("Login",
              style: TextStyle(
                color: Colors.black,
              )),
          style: ElevatedButton.styleFrom(
            primary: Colors.pink[200],
          ),
        ),
        ElevatedButton(
          onPressed: goBack,
          child: const Text("Back",
              style: TextStyle(
                color: Colors.black,
              )),
          style: ElevatedButton.styleFrom(
            primary: Colors.red[400],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        const Text(
          "Signin",
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.bold,
            decoration: TextDecoration.underline,
            color: Colors.lightGreenAccent,
          ),
        ),
        SizedBox(
          width: 300,
          child: _usernameTextarea(),
        ),
        const SizedBox(height: 10),
        SizedBox(
          width: 300,
          child: _passwordTextarea(),
        ),
        const SizedBox(height: 10),
        _rowButton(),
        Text(error, style: const TextStyle(color: Colors.red, fontSize: 16))
      ],
    ));
  }
}
