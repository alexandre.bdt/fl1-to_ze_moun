import 'package:flutter/material.dart';
import 'package:to_ze_moun/src/Page/Profile/profile_page.dart';
import 'package:to_ze_moun/src/Page/users_list.dart';
import 'package:to_ze_moun/src/Services/auth.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("To ze moun"),
        actions: [
          ElevatedButton.icon(
            onPressed: () => {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const ProfilePage()))
            },
            icon: const Icon(Icons.surfing, color: Colors.yellow),
            label: const Text("profile"),
          ),
          ElevatedButton(
            onPressed: () async => {await _auth.signout()},
            child: const Text("Signout"),
            style: ElevatedButton.styleFrom(
              shadowColor: Colors.black.withOpacity(0.0),
              elevation: 0.0,
            ),
          )
        ],
      ),
      body: const UserList(_auth.getUserName()),
    );
  }
}
