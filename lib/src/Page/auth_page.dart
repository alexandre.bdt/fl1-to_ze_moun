import 'package:flutter/material.dart';
import 'package:to_ze_moun/src/Page/login.dart';
import 'package:to_ze_moun/src/Page/register.dart';
import '../page_animation/rotation_transition.dart';

class AuthPage extends StatefulWidget {
  const AuthPage({Key? key}) : super(key: key);

  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  final ButtonStyle style = ElevatedButton.styleFrom(
    textStyle: const TextStyle(
      fontSize: 40,
      fontWeight: FontWeight.bold,
    ),
    primary: Colors.purple,
  );

  void openSignup() {
    Navigator.push(context, RotationRoute(page: const RegisterPage()));
  }

  void openSignin() {
    Navigator.push(context, RotationRoute(page: const SignInPage()));
  }

  Widget _title() {
    return const Text(
      "To ze moun",
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 40,
        fontFamily: 'Roboto',
        color: Color(0xfff7892b),
      ),
    );
  }

  Widget _moon(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Image.asset(
        "assets/images/moon.png",
        height: 300,
        width: 300,
      ),
    );
  }

  Widget _signinButton(BuildContext context) {
    return ElevatedButton(
      child: const Text("Login"),
      onPressed: openSignin,
      style: ElevatedButton.styleFrom(
        textStyle: const TextStyle(fontSize: 25),
        primary: Colors.purple,
      ),
    );
  }

  Widget _signupButton(BuildContext context) {
    return ElevatedButton(
      onPressed: openSignup,
      child: const Text(
        "Sign up",
        style: TextStyle(
          color: Colors.black,
          fontSize: 25,
          fontFamily: 'bold',
        ),
      ),
      style: ElevatedButton.styleFrom(
        primary: Colors.lime,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _moon(context),
          _title(),
          _signinButton(context),
          _signupButton(context)
        ],
      ),
    );
  }
}
