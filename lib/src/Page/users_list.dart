import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:to_ze_moun/src/Services/auth.dart';
import 'package:to_ze_moun/src/Services/messages_services.dart';
import 'package:to_ze_moun/src/Services/user.dart';
import 'package:to_ze_moun/src/Services/user_services.dart';

class UserList extends StatefulWidget {
  const UserList({Key? key}) : super(key: key);

  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  final AuthService _auth = AuthService();
  final messages = Messages("test");
  final _contoller = TextEditingController();

  void sendMessage(String text) {
    final message = Message(text, DateTime.now());
    messages.saveMessage(message);
    _contoller.clear();
    setState(() {});
  }

  Future<List<String>> getUsers() async {
    final reps = await UsersManagment().fetchUsers();
    final List<String> users = [];

    for (var i = 0; i < reps.length; i++) {
      users.add(reps[i].name);
    }
    return (users);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          FirebaseAnimatedList(
            query: messages.getMessageQuery(),
            itemBuilder: (context, snapshot, animation, index) {
              final js = snapshot.value as Map<dynamic, dynamic>;
              final message = Message.fromJson(js);
              return (Container(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 10, bottom: 10),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Text(message.text),
                ),
              ));
            },
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              padding: const EdgeInsets.only(left: 10, bottom: 10, top: 10),
              height: 60,
              width: double.infinity,
              color: Colors.white,
              child: Row(
                children: [
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                        hintText: "write",
                        border: InputBorder.none,
                      ),
                      controller: _contoller,
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  FloatingActionButton(
                    onPressed: () => {sendMessage(_contoller.text)},
                    child: const Icon(
                      Icons.send,
                      color: Colors.white,
                      size: 18,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
