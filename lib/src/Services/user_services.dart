import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class User {
  final String id;
  final String name;

  const User({
    required this.id,
    required this.name,
  });
}

class UsersManagment {
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('user');

  Future<List<User>> fetchUsers() async {
    QuerySnapshot res = await userCollection.get();

    List<User> users = [];

    for (int i = 0; i < res.docs.length; i++) {
      User tmp = User(id: res.docs[i]['id'], name: res.docs[i]['userName']);
      users.add(tmp);
    }

    return (users);
  }
}
