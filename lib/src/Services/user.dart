import 'package:firebase_auth/firebase_auth.dart';
import 'package:to_ze_moun/src/Services/storage.dart';

class UserService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final _store = Storage();

  Future uploadUserProfilePicture(file) async {
    User? user = _auth.currentUser;
    await _store.uploadFile(file, user!.uid);
    return;
  }

  Future<String?> getUserProfilePicture() async {
    User? user = _auth.currentUser;
    String? url = await _store.getUserProfilePicture(user!.uid);
    return url;
  }
}
