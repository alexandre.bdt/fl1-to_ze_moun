import 'package:firebase_storage/firebase_storage.dart';

class Storage {
  FirebaseStorage firebaseStorage = FirebaseStorage.instance;

  Future<void> uploadFile(file, uid) async {
    try {
      await firebaseStorage.ref().child("user/profile/$uid").putFile(file);
    } catch (e) {
      print(e);
    }
  }

  Future<String?> getUserProfilePicture(uid) async {
    try {
      return await firebaseStorage
          .ref()
          .child("user/profile/$uid")
          .getDownloadURL();
    } catch (e) {
      print(e.toString());
    }
  }
}
