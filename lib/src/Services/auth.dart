import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';

class AuthService {
  final userName = "";
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('user');

  Future signInWithEmail(String email, String password) async {
    try {
      UserCredential res = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      print(res);

      return res.user;
    } catch (e) {
      print(e.toString());
    }
  }

  Future createUserInDb(String email, UserCredential creds) async {
    try {
      return await userCollection.doc(email).set({
        'id': creds.user!.uid,
        'userName': email,
      });
    } catch (e) {
      print(e.toString());
    }
  }

  Future registerWithEmail(String email, String password) async {
    try {
      UserCredential res = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      return res;
    } catch (e) {
      print(e.toString());
      return e.toString();
    }
  }

  void setUserName(String userName) {
    userName = userName;
  }

  String getUserName() {
    return (userName);
  }

  Stream<User?> get user {
    return _auth.authStateChanges();
  }

  Future signout() async {
    return await _auth.signOut();
  }
}
