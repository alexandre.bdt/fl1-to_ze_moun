import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:to_ze_moun/src/Page/auth_page.dart';
import 'package:to_ze_moun/src/Page/home.dart';

class Wrapper extends StatefulWidget {
  const Wrapper({Key? key}) : super(key: key);

  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User?>(context);

    setState(() {});

    if (user == null) {
      return const AuthPage();
    } else {
      return const Home();
    }
  }
}
